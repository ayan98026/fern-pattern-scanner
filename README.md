# Fern Pattern Scanner

Welcome to Fern's Pattern Scanner. This scanner scans through your suggested files and finds any patterns stated within the [rules/rules.yaml](https://gitlab.com/gitlab-de/tutorials/security-and-governance/custom-scanner-integration/fern-pattern-scanner/-/blob/main/rules/rules.yaml) using [regex](https://en.wikipedia.org/wiki/Regular_expression). Then a report is generated with all the findings. This project is used to showcase how a custom [security scanner can be integrated in GitLab](https://docs.gitlab.com/ee/development/integrations/secure.html) can be used to populate the [Pipeline Security Tab](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/pipeline.html), the [vulnerability reports](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/) and the [merge request widget](https://docs.gitlab.com/ee/user/project/merge_requests/widgets.html)

**Note**: Accessing vulnerability reports requires a [GitLab Ultimate](https://about.gitlab.com/pricing/ultimate/) subscription

## Adding the scanner to your GitLab CI/CD pipeline

You can activate this security scanner by adding the following to your [.gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/):

```yaml
include:
  - remote: "https://gitlab.com/gitlab-de/tutorials/security-and-governance/custom-scanner-integration/fern-pattern-scanner/-/blob/main/jobs/.fern-pattern-scanner.gitlab-ci.yml" 
```

You can see the scanner run on the [Secret List](https://gitlab.com/gitlab-de/tutorials/security-and-governance/custom-scanner-integration/secret-list) project. Once the scanner completes, it generates a report named **gl-secret-detection-report.json**. That report is validated and loaded into the [vulnerability report](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/) and [merge request widget](https://docs.gitlab.com/ee/user/project/merge_requests/widgets.html).

If you wish to ignore scanning certain paths in the project the scanner is running on, you can set the following variable with comma separated values:

```yaml
variables:
  FERN_ANALYZER_EXCLUDED_PATHS: "docs, tests"
```

### Loading your own rule set

If you wish to run your own rule set of regex patterns in GitLab, you must do the following:

1. Create a [rules.yaml](https://gitlab.com/gitlab-de/tutorials/security-and-governance/custom-scanner-integration/fern-pattern-scanner/-/blob/main/rules/rules.yaml) file somewhere inside your project. The rules.yaml file should look as follows, where you can continue adding names and pattens as needed.

```yaml
patterns:
  - name: 'PATTERN NAME 1'
    pattern: 'REGEX 1'
  - name: 'PATTERN NAME 2'
    pattern: 'REGEX 2'
```

2. Override your .gitlab-ci.yml file with the following:

```yaml
fern_secret_detection:
  script:
    - /app/fern-pattern-scanner scan --rules "/path/to/rules.yaml" --report
```

3. Check in your code and the scanner should begin running with the new rule set. This scanner does not scan commit diffs. It searches for patterns across the entire project

4. When the pipeline completes, you can click on the [Security tab](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/pipeline.html) to view all the detected issues. If you have GitLab Ultimate, these issues can also be seen in the [Vulnerability Report](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/)

## Running the scanner locally

You can use this scanner to detect patterns for selected file types on your local machine. In order to run this scanner on your local machine follow the steps below:

1. Install the scanner

```bash
$ go install gitlab.com/gitlab-de/tutorials/security-and-governance/custom-scanner-integration/fern-pattern-scanner
```

**Note**: This will download the file to your $GOBIN. If you are new to Go, this [playground](https://play-with-go.dev/installing-go_go119_en/) goes over the GO installation

2. Download the [rules.yaml](https://gitlab.com/gitlab-de/tutorials/security-and-governance/custom-scanner-integration/fern-pattern-scanner/-/blob/main/rules/rules.yaml) file

```bash
$ wget https://gitlab.com/gitlab-de/tutorials/security-and-governance/custom-scanner-integration/fern-pattern-scanner/-/raw/main/rules/rules.yaml

...
Resolving gitlab.com (gitlab.com)... 2606:4700:90:0:f22e:fbec:5bed:a9b9, 172.65.251.78
Connecting to gitlab.com (gitlab.com)|2606:4700:90:0:f22e:fbec:5bed:a9b9|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 283 [text/plain]
Saving to: ‘rules.yaml’

rules.yaml          100%[===================>]     283  --.-KB/s    in 0s

2023-05-20 16:02:46 (22.5 MB/s) - ‘rules.yaml’ saved [283/283]
```

**Note**: You can also download the file directly from the [project](https://gitlab.com/gitlab-de/tutorials/security-and-governance/custom-scanner-integration/fern-pattern-scanner/-/blob/main/rules/rules.yaml)

3. Run the scanner with the rules file

```bash
$ fern-pattern-scanner scan --rules path/to/rules.yaml

INFO[0000] The scanner is starting                      
INFO[0000] Skipping the following directories: []       
INFO[0000] Possible Vulnerability Detected: {Id:139999 Name:password variation 1 Message:The pattern in rule: password variation 1 was found in a file Description:The pattern in rule: password variation 1 was found in a file. Examine the file, remove and rotate your secret. Location:/Users/fern/Desktop/secret-list/.git/index Line:3} 
...
INFO[0000] The scanner completed successfully
```

---

Created and maintained by [Fern](https://gitlab.com/fjdiaz)🌿

- [LinkedIn](https://www.linkedin.com/in/awkwardferny/)
- [Twitter](https://twitter.com/awkwardferny)