package scanner

type Rules struct {
	Patterns []Pattern `yaml:"patterns"` // List of patterns
}

type Pattern struct {
	Name    string `yaml:"name"`    // Name of pattern
	Pattern string `yaml:"pattern"` // The regex
}

type Report struct {
	Start           string          // Start time of scan
	End             string          // End time of scan
	Vulnerabilities []Vulnerability // List of vulnerabilities
	Commit          Commit          // Commit information needed in schema
	Status          string          // Success or failure
	Version         string          // The version of this application
}

type Vulnerability struct {
	Id          string // Randomly generated ID
	Name        string // Name of the vulnerability
	Message     string // Message to display to user
	Description string // Description of the vulnerability
	Location    string // File where vulnerability was detected
	Line        int    // Line of code where the vulnerability was detected
}

type Commit struct {
	Author  string // CI_COMMIT_AUTHOR env variable in GitLab CI Job
	Date    string // CI_JOB_STARTED_AT env variable in GitLab CI Job
	Message string // CI_COMMIT_MESSAGE env variable in GitLab CI Job
	SHA     string // CI_COMMIT_SHA env variable in GitLab CI Job
}
