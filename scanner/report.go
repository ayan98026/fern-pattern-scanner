package scanner

import (
	"html/template"
	"log"
	"os"
	"path/filepath"
)

var reportName = "gl-secret-detection-report.json"
var reportTemplate = `{{ $report := . -}}
{{ $commit := .Commit -}}
{{ $vulns := .Vulnerabilities -}}
{{ $length := len $vulns -}}
{{ $addcomma := minusOne $length -}}
{
    "version":"15.0.6",
    "vulnerabilities":[
      {{- range $index, $vuln := $vulns }}
       {
          "id":"{{ $vuln.Id }}",
          "category":"secret_detection",
          "name":"{{ $vuln.Name }}",
          "message":"{{ $vuln.Message }}",
          "description":"{{ $vuln.Description }}",
          "cve":"NOT IMPLEMENTED",
          "severity":"Critical",
          "confidence":"Unknown",
          "raw_source_code_extract":"NOT IMPLEMENTED",
          "scanner":{
             "id":"Fern Pattern Scanner",
             "name":"Fern Pattern Scanner"
          },
          "location":{
             "file":"{{ $vuln.Location }}",
             "commit":{
                "author":"{{ $commit.Author }}",
                "date":"{{ $commit.Date }}",
                "message":"{{ $commit.Message }}",
                "sha":"{{ $commit.SHA }}"
             },
             "start_line":{{ $vuln.Line }}
          },
          "identifiers":[
             {
                "type":"fern_pattern_rule",
                "name":"{{ $vuln.Name }}",
                "value":"{{ $vuln.Description }}"
             }
          ]
       }{{ if lt $index $addcomma }},{{ end }}{{ end }}
    ],
    "dependency_files":[],
    "scan":{
       "analyzer":{
          "id":"fern_pattern_scanner",
          "name":"fern_pattern_scanner",
          "url":"https://gitlab.com/gitlab-de/tutorials/security-and-governance/custom-scanner-integration/fern-pattern-scanner",
          "vendor":{
             "name":"GitLab"
          },
          "version":"{{ $report.Version }}"
       },
       "scanner":{
          "id":"fern_pattern_scanner",
          "name":"fern_pattern_scanner",
          "url":"https://gitlab.com/gitlab-de/tutorials/security-and-governance/custom-scanner-integration/fern-pattern-scanner",
          "vendor":{
             "name":"GitLab"
          },
          "version":"{{ $report.Version }}"
       },
       "type":"secret_detection",
       "start_time":"{{ $report.Start }}",
       "end_time":"{{ $report.End }}",
       "status":"{{ $report.Status }}"
    }
}`

// Add functions to template
var funcMap = template.FuncMap{
	"minusOne": minusOne,
}

// Accepts scanner_results and then generates a report from that
func GenerateReport(report Report) error {
	// Load template from string
	tmpl, err := template.New("fern-pattern-scanner.tmpl").Funcs(funcMap).Parse(reportTemplate)
	if err != nil {
		log.Fatal(err)
	}

	// Load current directory to scan
	currentDir, err := filepath.Abs("")
	if err != nil {
		log.Fatal(err)
	}

	// Create a file to save the template
	file, err := os.Create(currentDir + "/" + reportName)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	// Execute the template using the report
	err = tmpl.Execute(file, report)
	if err != nil {
		log.Fatal(err)
	}

	return nil
}

// Used to subtract one from index for parsing parenthesis
func minusOne(index int) int {
	return index - 1
}
